﻿using UnityEngine;

public class Food : MonoBehaviour
{
    public Transform topBorder;
    public Transform bottomBorder;
    public Transform leftBorder;
    public Transform rightBorder;

    public GameObject grass;

    public bool m_eaten = false;

    //Getters and Setters
    public bool eaten
    {
        get { return m_eaten; }
        set { m_eaten = value; }
    }

    //Spawn food randomly between the borders of the game
    void SpawnFood()
    {
        int randomX = (int)Random.Range(leftBorder.position.x+2, rightBorder.position.x-2);
        int randomY = (int)Random.Range(topBorder.position.y-2, bottomBorder.position.y+2);
        Vector2 position = new Vector2(randomX, randomY);
        
        Instantiate(gameObject, position, Quaternion.identity);
    }

    void AddScore()
    {
        Score.score++;
    }

    //Check if collided with player
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If so, add score, spawn new food and destroy the food that player collided with 
        if (collision.CompareTag("Player"))
        {
            eaten = true;
            AddScore();
            SpawnFood();
            Destroy(gameObject);
        }
    }
}