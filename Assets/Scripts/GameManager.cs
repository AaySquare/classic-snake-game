﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private void Start()
    {
        //As soon as the game begins, freeze time (To display controls)
        if (SceneManager.GetActiveScene().name == "Game")
        {
            Time.timeScale = 0;
        }
    }

    private void Update()
    {
        //In the game scene, display the game over and controls canvas
        if (SceneManager.GetActiveScene().name == "Game")
        {
            DisplayControls();
            DisplayGameOver();
        }

        //In the menu scene, press spacebar to start the game
        else if (SceneManager.GetActiveScene().name == "Menu")
        {
            if (Input.GetKey(KeyCode.Space))
            {
                BeginGame();
            }
        }
    }

    //Disable controls display once user has pressed any key and unfreeze the time
    void DisplayControls()
    {
        if (Input.anyKey)
        {
            Time.timeScale = 1;
            GameObject.FindGameObjectWithTag("Controls").GetComponent<Canvas>().enabled = false;
        }
    }

    //Display Gameover canvas if it is Gameover
    void DisplayGameOver()
    {
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Snake>().gameOver)
        {
            GameObject.FindGameObjectWithTag("GameOverCanvas").GetComponent<Canvas>().enabled = true;
        }
    }

    public void BeginGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void BeginMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
