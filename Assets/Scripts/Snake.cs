﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Snake : MonoBehaviour
{
    //Starting movement direction
    Vector2 m_direction = Vector2.right;

    //Speed at which snake moves
    private float m_speed = 0.2f;

    //To check whether the game is over or not
    private bool m_GameOver = false;

    //To create a list of tails
    private List<Transform> tails = new List<Transform>();

    public GameObject tail;

    //Getters and Setters
    public bool gameOver
    {
        get { return m_GameOver; }
        set { m_GameOver = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Call to Movement every specified seconds instead of every frame (Otherwise it's too fast!)
        InvokeRepeating("Movement", 1.0f, m_speed);
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
    }

    //Player Input
    void HandleInput()
    {
        //Player controls
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            if(m_direction != Vector2.left || tails.Count == 0)
            {
                m_direction = Vector2.right;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            if (m_direction != Vector2.right || tails.Count == 0)
            {
                m_direction = Vector2.left;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            if (m_direction != Vector2.down || tails.Count == 0)
            {
                m_direction = Vector2.up;
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            if (m_direction != Vector2.up || tails.Count == 0)
            {
                m_direction = Vector2.down;
            }
        }
    }

    void Movement()
    {
        //If it is not game over, keep moving the snake head and tails
        if (!gameOver)
        {
            //Get snake head's position before moving it
            Vector2 previousHeadPosition = transform.position;

            //To move game object 1 unit at a time
            transform.Translate(m_direction);

            //If food is eaten, spawn a new tail at the last location of the snake head
            if (GameObject.FindGameObjectWithTag("Food").GetComponent<Food>().eaten)
            {
                GameObject newTail = Instantiate(tail, previousHeadPosition, Quaternion.identity);
                tails.Insert(0, newTail.transform);
                GameObject.FindGameObjectWithTag("Food").GetComponent<Food>().eaten = false;
            }

            //Check if there is any tail
            if (tails.Count > 0)
            {
                //Set the position of the last tail to be previous position of snake head
                tails.Last().position = previousHeadPosition;

                //Set the last tail to be at the front
                tails.Insert(0, tails.Last());

                //Remove the last tail
                tails.RemoveAt(tails.Count - 1);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Check for collision with the borders and tail
        if(collision.CompareTag("Border") || collision.gameObject.CompareTag("Tail"))
        {
            //Game Over
            gameOver = true;
        }
    }
}
