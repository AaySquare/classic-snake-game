﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static int score;

    public Text scoreText;
    public Text bestScoreText;

    void Start()
    {
        //Start the game with best score being displayed
        bestScoreText.text = "Best Score \n      " + PlayerPrefs.GetInt("BestScore", 0).ToString();

        //Reset current score to 0 everytime game starts
        score = 0;
    }

    void Update()
    {
        UpdateScoreText();
    }

    //Update score and best score texts
    void UpdateScoreText()
    {
        scoreText.text = "Score \n    " + score.ToString();

        if(score > PlayerPrefs.GetInt("BestScore", 0))
        {
            PlayerPrefs.SetInt("BestScore", score);
            bestScoreText.text = "Best Score \n      " + score.ToString();
        }
    }
}
